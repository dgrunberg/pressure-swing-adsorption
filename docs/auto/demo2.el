(TeX-add-style-hook
 "demo2"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "12pt")))
   (TeX-run-style-hooks
    "latex2e"
    "article"
    "art12"
    "amsmath"
    "amsfonts"
    "amssymb"
    "amsthm"
    "graphicx")
   (LaTeX-add-bibitems
    "rabin"
    "coding"
    "ffm")
   (LaTeX-add-amsthm-newtheorems
    "remark"))
 :latex)

