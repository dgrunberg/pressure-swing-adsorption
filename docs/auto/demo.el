(TeX-add-style-hook
 "demo"
 (lambda ()
   (TeX-add-to-alist 'LaTeX-provided-class-options
                     '(("article" "12pt")))
   (TeX-run-style-hooks
    "latex2e"
    "ke138-time1"
    "article"
    "art12"
    "amsmath"
    "amsfonts"
    "amssymb"
    "amsthm"
    "graphicx")
   (TeX-add-symbols
    '("del" 2)
    '("delt" 1)
    "svgwidth")
   (LaTeX-add-labels
    "fig:blockdiagram"
    "comp"
    "total")
   (LaTeX-add-bibitems
    "haghpanah"
    "jee"
    "farooq"
    "santos")
   (LaTeX-add-amsthm-newtheorems
    "remark"))
 :latex)

